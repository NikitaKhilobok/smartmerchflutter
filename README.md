
# smartmerchflutter plugin

  

Smart Merch flutter package for IOS and Andoid

  

## Installation

First, add `smartmerchflutter` as a dependency in your pubspec.yaml file.
*Note:* 
Because package not published on pub, use git path instead:

    smartmerchflutter:
	    git: https://gitlab.com/NikitaKhilobok/smartmerchflutter.git

### IOS
##### IOS 10 or higher
Add two rows to the  `ios/Runner/Info.plist`:

-   one with the key  `Privacy - Camera Usage Description`  and a usage description.
-   and one with the key  `Privacy - Microphone Usage Description`  and a usage description.

Or in text format add the key:

    <key>NSCameraUsageDescription</key>
    <string>Can I use the camera please?</string>
    <key>NSMicrophoneUsageDescription</key>
    <string>Can I use the mic please?</string>
### Android
##### minSdk 21 or higher
It's important to note that the `MediaRecorder` class is not working properly on emulators, as stated in the documentation: [https://developer.android.com/reference/android/media/MediaRecorder](https://developer.android.com/reference/android/media/MediaRecorder). Specifically, when recording a video with sound enabled and trying to play it back, the duration won't be correct and you will only see the first frame.